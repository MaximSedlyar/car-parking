# Car Parking Service - NestJS

## Getting started

1. `docker-compose up -d --build`
2. You cat use `http://127.0.0.1:3000/graphql` in browser or in Postman

### Mutations

1. Create a car

```
createCar(data: {
    brand: "bmw",
    model: "328d",
    year: "2013",
    fuelType: DIESEL
}){
    id
}
```

2. Create a driver

```
createDriver(data: {
    firstName: "Maksym",
    lastName: "Sedliar",
}){
    id
}
```

3. Set car to driver

```
setCarToDriver(data: {
    driverId: 1,
    carId: 1
}) {
    id
}
```

### Query

1. Cars with drivers

```
cars {
    id
    brand
    model
    year
    drivers{
        id
        fullName
    }
}
```
