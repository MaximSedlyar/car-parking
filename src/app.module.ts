import { Module } from '@nestjs/common';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CarModule } from './car/car.module';
import { GraphQLModule } from '@nestjs/graphql';
import { GraphQLError } from 'graphql';
import { GraphQLResponse } from 'apollo-server-types';
import { DriverModule } from './driver/driver.module';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get('DATABASE_HOST', 'localhost'),
        port: configService.get<number>('DATABASE_PORT', 3306),
        username: configService.get('DATABASE_USER', 'car'),
        password: configService.get('DATABASE_PASS', 'car'),
        database: configService.get('DATABASE_NAME', 'car_parking'),
        entities: ['dist/**/entities/**.entity{.ts,.js}'],
        synchronize: true,
        logging: true,
      }),
    }),
    CarModule,
    DriverModule,
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: 'schema.gql',
      formatResponse: (response: GraphQLResponse) => {
        if (response.errors && Array.isArray(response.errors[0])) {
          response.errors = response.errors[0];
        }
        return response;
      },
      formatError: (e: GraphQLError) => {
        try {
          const errors = JSON.parse(e?.message);
          return errors.map((message) => {
            return {
              message,
              code: e?.extensions?.code,
            };
          });
        } catch (err) {
          return {
            message: e?.message,
            code: e?.extensions?.code,
          };
        }
      },
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
