import * as Joi from 'joi';

export default {
  createDriver: Joi.object({
    firstName: Joi.string().required().max(50),
    lastName: Joi.string().required().max(50),
  }),
  setCarToDriver: Joi.object({
    driverId: Joi.number().required(),
    carId: Joi.number().required(),
  }),
};
