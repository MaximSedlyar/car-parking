import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Driver } from './entities/driver.entity';
import { DriverService } from './services/driver.service';
import { DriverResolver } from './resolvers/driver.resolver';
import { CarModule } from '../car/car.module';

@Module({
  imports: [CarModule, TypeOrmModule.forFeature([Driver])],
  providers: [DriverService, DriverResolver],
  exports: [DriverService],
})
export class DriverModule {}
