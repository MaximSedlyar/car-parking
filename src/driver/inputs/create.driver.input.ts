import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class CreateDriverInput {
  @Field()
  firstName: string;

  @Field({ nullable: true })
  lastName: string;

  @Field({ nullable: true })
  isActive: boolean;
}
