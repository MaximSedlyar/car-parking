import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class SetCarInput {
  @Field()
  driverId: number;

  @Field()
  carId: number;
}
