import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  ManyToOne,
} from 'typeorm';
import { ObjectType, Field } from '@nestjs/graphql';
import { Expose } from 'class-transformer';
import { Car } from '../../car/entities/car.entity';

@ObjectType()
@Entity()
export class Driver {
  @Field()
  @PrimaryGeneratedColumn()
  public id: number;

  @Field()
  @Column({ name: 'first_name', length: 50 })
  public firstName: string;

  @Field()
  @Column({ name: 'last_name', length: 50 })
  public lastName: string;

  @Field()
  @Column({ name: 'is_active', default: true })
  public isActive: boolean;

  @CreateDateColumn({ name: 'created_at' })
  public createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  public updatedAt: Date;

  @Field()
  @Expose()
  get fullName(): string {
    return `${this.firstName} ${this.lastName}`;
  }

  @ManyToOne(() => Car, (car) => car.drivers)
  car: Car;
}
