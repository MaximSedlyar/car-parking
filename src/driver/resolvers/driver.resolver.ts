import { Resolver, Args, Query, Mutation } from '@nestjs/graphql';
import { Inject, UsePipes } from '@nestjs/common';
import { DriverService } from '../services/driver.service';
import { CarService } from '../../car/services/car.service';
import { Driver as DriverModel } from '../entities/driver.entity';
import { Car as CarModel } from '../../car/entities/car.entity';
import { CreateDriverInput } from '../inputs/create.driver.input';
import { SetCarInput } from '../inputs/set.car.input';
import { JoiValidationPipe } from '../../joi.validation.pipe';
import validationSchema from '../validation.rules';

@Resolver(() => DriverModel)
export class DriverResolver {
  constructor(
    @Inject(DriverService) private driverService: DriverService,
    @Inject(CarService) private carService: CarService,
  ) {}

  @Query(() => DriverModel)
  async driver(@Args('id') id: number): Promise<DriverModel> {
    return await this.driverService.getDriver(id);
  }

  @Query(() => [DriverModel])
  async drivers(): Promise<DriverModel[]> {
    return this.driverService.getDrivers();
  }

  @Mutation(() => DriverModel)
  @UsePipes(new JoiValidationPipe(validationSchema.createDriver))
  async createDriver(@Args('data') createDriverInput: CreateDriverInput) {
    return await this.driverService.create(createDriverInput);
  }

  @Mutation(() => DriverModel)
  @UsePipes(new JoiValidationPipe(validationSchema.setCarToDriver))
  async setCarToDriver(@Args('data') setCarInput: SetCarInput) {
    const car: CarModel = await this.carService.getCarWithDrivers(
      setCarInput.carId,
    );
    const driver: DriverModel = await this.driverService.getDriver(
      setCarInput.driverId,
    );
    driver.car = car;
    return this.driverService.save(driver);
  }
}
