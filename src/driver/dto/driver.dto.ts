export class DriverDTO {
  firstName: string;
  lastName: string;
  isActive: boolean;
}
