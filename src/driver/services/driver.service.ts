import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Driver } from '../entities/driver.entity';
import { DriverDTO } from '../dto/driver.dto';

@Injectable()
export class DriverService {
  constructor(
    @InjectRepository(Driver) private driversRepository: Repository<Driver>,
  ) {}

  getDrivers(): Promise<Driver[]> {
    return this.driversRepository.find();
  }

  getDriver(id: number): Promise<Driver> {
    return this.driversRepository.findOne(id);
  }

  create(driver: DriverDTO): Promise<Driver> {
    const newDriver = this.driversRepository.create({
      firstName: driver.firstName,
      lastName: driver.lastName,
      isActive: driver.isActive,
    });
    return this.driversRepository.save(newDriver);
  }

  save(driver: Driver): Promise<Driver> {
    return this.driversRepository.save(driver);
  }
}
