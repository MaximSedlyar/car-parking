import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { ObjectSchema } from 'joi';

@Injectable()
export class JoiValidationPipe implements PipeTransform {
  constructor(private schema: ObjectSchema) {}

  transform(value: any) {
    const { error } = this.schema.validate(value, { abortEarly: false });
    if (error) {
      const errors = [];
      error.details.map((detail) => errors.push(detail.message));
      throw new BadRequestException(JSON.stringify(errors));
    }
    return value;
  }
}
