import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Car } from '../entities/car.entity';
import { CarDTO } from '../dto/car.dto';

@Injectable()
export class CarService {
  constructor(@InjectRepository(Car) private carsRepository: Repository<Car>) {}

  async getCars(): Promise<Car[]> {
    return this.carsRepository.find({
      relations: ['drivers'],
    });
  }

  getCar(id: number): Promise<Car> {
    return this.carsRepository.findOne(id);
  }

  getCarWithDrivers(id: number): Promise<Car> {
    return this.carsRepository.findOne(id, {
      relations: ['drivers'],
    });
  }

  create(car: CarDTO): Promise<Car> {
    const newCar = this.carsRepository.create({
      brand: car.brand,
      model: car.model,
      fuelType: car.fuelType,
      year: car.year,
      isActive: car.isActive,
    });
    return this.carsRepository.save(newCar);
  }
}
