import { registerEnumType } from '@nestjs/graphql';

export enum FuelTypes {
  GAS = 'GAS',
  DIESEL = 'DIESEL',
}

registerEnumType(FuelTypes, {
  name: 'FuelTypes',
});
