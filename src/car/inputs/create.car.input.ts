import { Field, InputType } from '@nestjs/graphql';
import { FuelTypes } from '../enums/fuel.types.enum';

@InputType()
export class CreateCarInput {
  @Field()
  brand: string;

  @Field({ nullable: true })
  model: string;

  @Field(() => FuelTypes, { nullable: true })
  fuelType: FuelTypes;

  @Field()
  year: string;

  @Field({ nullable: true })
  isActive: boolean;
}
