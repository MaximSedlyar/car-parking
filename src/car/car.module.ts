import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CarService } from './services/car.service';
import { CarResolver } from './resolvers/car.resolver';
import { Car } from './entities/car.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Car])],
  providers: [CarService, CarResolver],
  exports: [CarService],
})
export class CarModule {}
