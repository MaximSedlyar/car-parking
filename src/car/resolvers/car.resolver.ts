import { Resolver, Args, Query, Mutation } from '@nestjs/graphql';
import { Inject, UsePipes } from '@nestjs/common';
import { CarService } from '../services/car.service';
import { Car as CarModel } from '../entities/car.entity';
import { CreateCarInput } from '../inputs/create.car.input';
import { JoiValidationPipe } from '../../joi.validation.pipe';
import validationSchema from '../validation.rules';

@Resolver(() => CarModel)
export class CarResolver {
  constructor(@Inject(CarService) private carService: CarService) {}

  @Query(() => CarModel)
  async car(@Args('id') id: number): Promise<CarModel> {
    return await this.carService.getCar(id);
  }

  @Query(() => [CarModel])
  async cars(): Promise<CarModel[]> {
    return this.carService.getCars();
  }

  @Mutation(() => CarModel)
  @UsePipes(new JoiValidationPipe(validationSchema))
  async createCar(@Args('data') createCarInput: CreateCarInput) {
    return await this.carService.create(createCarInput);
  }
}
