import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';
import { ObjectType, Field } from '@nestjs/graphql';
import { Driver } from '../../driver/entities/driver.entity';

@ObjectType()
@Entity()
export class Car {
  @Field()
  @PrimaryGeneratedColumn()
  public id: number;

  @Field()
  @Column({ length: 50 })
  public brand: string;

  @Field()
  @Column({ length: 50, default: '' })
  public model: string;

  @Field()
  @Column({ name: 'fuel_type', length: 15 })
  public fuelType: string;

  @Field()
  @Column({ length: 4 })
  public year: string;

  @Field()
  @Column({ name: 'is_active', default: true })
  public isActive: boolean;

  @CreateDateColumn({ name: 'created_at' })
  public createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  public updatedAt: Date;

  @Field(() => [Driver], { nullable: true })
  @OneToMany(() => Driver, (driver) => driver.car)
  drivers: Driver[];
}
