import * as Joi from 'joi';

export default Joi.object({
  year: Joi.string().required().length(4),
  brand: Joi.string().required(),
  model: Joi.string().allow(''),
  fuelType: Joi.string().required(),
});
