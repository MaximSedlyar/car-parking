export class CarDTO {
  brand: string;
  model: string;
  fuelType: string;
  year: string;
  isActive: boolean;
}
