FROM node:16-alpine
RUN apk add --no-cache curl

# Create app directory
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY  . .

RUN npm run build

CMD [ "node", "dist/main" ]